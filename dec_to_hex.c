#include <stdio.h>

int main(int argc, char **argv){

    //argc number of command line arguments passed in.

    int dec[14] = {38,172,144,85,424,111,174,551,446,32,428,544,96,170};
    int hex = 0;        

    for(int i=0; i<14; i++){

        hex = dec[i];
        printf("Bas-10: %d\tBas-16:  %X \n",dec[i], hex);
    }

    return 0;
}

