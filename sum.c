#include <stdio.h>
#include <assert.h>

int main (int argc, char *argv[]){
	int sum = 0;
	int input = 0;
	
	printf("Skriv en siffra att summera till: ");

	scanf("%d", &input);

	assert(input <= 32767);

	for(int i=0; i<input; i++){
		sum = i + sum;
	}
	printf("%d\n", sum);
	

}

